package bootc

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"bootc/pkg/config"
	"bootc/pkg/podman"
)

func Install(imageId, vmDir string) error {
	// Create a raw disk image
	// TODO: Add disk size option
	diskFilePath := filepath.Join(vmDir, config.BootcDiskImage)
	diskFile, err := os.Create(diskFilePath)
	if err != nil {
		return err
	}
	// just ~5GB
	if err := diskFile.Truncate(5e+9); err != nil {
		return err
	}

	// Installing
	// https://github.com/containers/bootc/blob/main/docs/install.md#using-bootc-install-to-disk---via-loopback
	volumeBind := fmt.Sprintf("%s:/output", vmDir)
	installArgsForPodman := []string{"sudo", "podman", "run", "--rm", "--privileged", "--pid=host", "-v", volumeBind, "--security-opt", "label=type:unconfined_t"}
	if val, ok := os.LookupEnv("PODMAN_BOOTC_INST_ARGS"); ok {
		parts := strings.Split(val, " ")
		installArgsForPodman = append(installArgsForPodman, parts...)
	}

	installArgsForPodman = append(installArgsForPodman, imageId)
	installArgsForBootc := []string{"bootc", "install", "to-disk", "--via-loopback", "--generic-image", "--skip-fetch-check", "/output/" + config.BootcDiskImage}

	if err := podman.RunOnDefaultMachine(append(installArgsForPodman, installArgsForBootc...)); err != nil {
		return fmt.Errorf("failed to generate disk image via bootc install to-disk --via-loopback")
	}

	return nil
}
