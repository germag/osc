package config

import (
	"encoding/json"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

const (
	runBaseDir = "/run/user"
	configDir  = ".config"
	cacheDir   = ".cache"

	projectName             = "podman-bootc"
	RunPidFile              = "run.pid"
	OciArchiveOutput        = "image-archive.tar"
	BootcDiskImage          = "disk.img"
	BootcCiDataIso          = "cidata.iso"
	BootcCiDefaultTransport = "cdrom"
	BootcSshKeyFile         = "sshkey"
	BootcCfgFile            = "bc.cfg"
)

var (
	User, _   = user.Current()
	SshDir    = filepath.Join(User.HomeDir, ".ssh")
	ConfigDir = filepath.Join(User.HomeDir, configDir)
	RunDir    = filepath.Join(runBaseDir, User.Uid, projectName)

	CacheDir        = filepath.Join(User.HomeDir, cacheDir, projectName)
	MachineCacheDir = filepath.Join("/home/core", cacheDir, projectName)
	DefaultIdentity = filepath.Join(User.HomeDir, ".ssh", "id_rsa")
)

// VM files
type BcVmConfig struct {
	SshPort     int    `json:"SshPort"`
	SshIdentity string `json:"SshPriKey"`
}

// VM Status
const (
	Installing string = "Installing"
	Running           = "Running"
	Stopped           = "Stopped"
)

type RunVmConfig struct {
	SshPort uint64 `json:"SshPort"`
	VncPort uint64 `json:"VncPort"`
}

type VmConfig struct {
	Name       string `json:"Name"`
	Vcpu       uint64 `json:"VCPU"`
	Mem        uint64 `json:"Mem"`
	DiskSize   uint64 `json:"DiskSize"`
	DiskImage  string `json:"DiskImage"`
	RunPidFile string `json:"RunPidFile"`
	SshPriKey  string `json:"SshPriKey"`
}

func InitOSCDirs() error {
	if err := os.MkdirAll(ConfigDir, os.ModePerm); err != nil {
		return err
	}
	if err := os.MkdirAll(CacheDir, os.ModePerm); err != nil {
		return err
	}

	if err := os.MkdirAll(RunDir, os.ModePerm); err != nil {
		return err
	}

	return nil
}

// FIXME: Remove
func BootcImagePath(id string) (string, error) {
	files, err := os.ReadDir(CacheDir)
	if err != nil {
		return "", err
	}

	imageId := ""
	for _, f := range files {
		if f.IsDir() && strings.HasPrefix(f.Name(), id) {
			imageId = f.Name()
		}
	}

	if imageId == "" {
		return "", fmt.Errorf("local installation '%s' does not exists", id)
	}

	return filepath.Join(CacheDir, imageId), nil
}

func LoadConfig(id string) (*BcVmConfig, error) {
	vmPath, err := BootcImagePath(id)
	if err != nil {
		return nil, err
	}

	cfgFile := filepath.Join(vmPath, BootcCfgFile)
	fileContent, err := os.ReadFile(cfgFile)
	if err != nil {
		return nil, err
	}

	cfg := new(BcVmConfig)
	if err := json.Unmarshal(fileContent, cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}

func WriteConfig(vmDir string, sshPort int, sshIdentity string) error {
	bcConfig := BcVmConfig{SshPort: sshPort, SshIdentity: sshIdentity}
	bcConfigMsh, err := json.Marshal(bcConfig)
	if err != nil {
		return fmt.Errorf("marshal config data: %w", err)
	}
	cfgFile := filepath.Join(vmDir, BootcCfgFile)
	err = os.WriteFile(cfgFile, bcConfigMsh, 0660)
	if err != nil {
		return fmt.Errorf("write config file: %w", err)
	}
	return nil
}
