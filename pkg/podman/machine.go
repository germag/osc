package podman

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"bootc/pkg/config"
)

func RunOnDefaultMachine(cmdArgs []string) error {
	finalArgs := append([]string{"machine", "ssh"}, cmdArgs...)
	c := PodmanRecurse(finalArgs)
	env := c.Environ()
	// Remove CONTAINER_CONNECTION because it confuses podman machine ssh.
	// I set this in my toolbox to automatically connect podman
	newEnv := []string{}
	for _, elt := range env {
		if !strings.HasPrefix(elt, "CONTAINER_CONNECTION=") {
			newEnv = append(newEnv, elt)
		}
	}
	c.Stderr = os.Stderr
	c.Env = newEnv
	if err := c.Run(); err != nil {
		return fmt.Errorf("failed to run %v via podman machine ssh: %w", cmdArgs, err)
	}
	return nil
}

func IsDefaultMachineRunning() bool {
	c := exec.Command("podman", []string{"machine", "inspect", "--format", "{{.State}}"}...)
	buf := &bytes.Buffer{}
	c.Stdout = buf
	if err := c.Run(); err != nil {
		return false
	}
	return strings.TrimSpace(buf.String()) == "running"
}

func LoadImageToMachineRoot(imageId string, local bool) error {
	// Create oci image cache dir on podman machine
	imgDir := filepath.Join(config.MachineCacheDir, imageId)
	if err := RunOnDefaultMachine([]string{"mkdir", "-p", imgDir}); err != nil {
		return err
	}

	// Save the image to the cache
	output := filepath.Join(config.MachineCacheDir, imageId, config.OciArchiveOutput)
	if local {
		output = filepath.Join(config.CacheDir, imageId, config.OciArchiveOutput)
	}

	err := saveImageToOciArchive(imageId, output, local)
	if err != nil {
		return err
	}

	// Load the image to the podman machine VM
	cmd := []string{"sudo", "podman", "load", "-i", output}
	if err := RunOnDefaultMachine(cmd); err != nil {
		return fmt.Errorf("load oci image: %w", err)
	}

	// 'bootc install' fails if the image is not tagged
	cmd = []string{"sudo", "podman", "tag", imageId, imageId}
	if err := RunOnDefaultMachine(cmd); err != nil {
		return fmt.Errorf("tag oci image: %w", err)
	}
	return nil
}

func saveImageToOciArchive(imageId, output string, local bool) error {
	args := []string{"save", "--format", "oci-archive", "-o", output, imageId}
	if local {
		cmd := exec.Command("podman", args...)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		return err
	}

	return RunOnDefaultMachine(append([]string{"podman"}, args...))
}
