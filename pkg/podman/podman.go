package podman

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
)

func PodmanRecurse(args []string) *exec.Cmd {
	c := exec.Command("podman", args...)
	return c
}

func PodmanRecurseRun(args []string) error {
	c := PodmanRecurse(args)
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	logrus.Debugf("Running %v", args)
	return c.Run()
}

func GetOciImage(idOrName string, local bool) (string, error) {
	// Run an inspect to see if the image is present, otherwise pull.
	// TODO: Add podman pull --if-not-present or so.
	imageId, err := GetOciImageId(idOrName, true, local)
	if err != nil {
		logrus.Debugf("Inspect failed: %v", err)
		podmanPullArgs := []string{"-r", "pull", idOrName}
		if local {
			podmanPullArgs = podmanPullArgs[1:]
		}
		if err := PodmanRecurseRun(podmanPullArgs); err != nil {
			return "", fmt.Errorf("pulling image: %w", err)
		}
	}

	imageId, err = GetOciImageId(idOrName, false, local)
	if err != nil {
		return "", err
	}
	return imageId, nil
}

// podman image inspect --format "{{.Id}} {{index .RepoTags 0}}" 57f94b113863
func GetOciImageId(idOrName string, quiet, local bool) (string, error) {
	podmanImageArgs := []string{"-r", "image", "inspect", "-f", "{{.Id}}", idOrName}
	if local {
		podmanImageArgs = podmanImageArgs[1:]
	}

	buf := &bytes.Buffer{}
	c := PodmanRecurse(podmanImageArgs)
	c.Stdout = buf
	if !quiet {
		c.Stderr = os.Stderr
	}
	if err := c.Run(); err != nil {
		return "", fmt.Errorf("failed to inspect %s: %w", idOrName, err)
	}
	imageId := strings.TrimSpace(buf.String())
	return imageId, nil
}
