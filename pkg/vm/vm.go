package vm

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"bootc/pkg/config"
	"bootc/pkg/utils"

	"github.com/fsnotify/fsnotify"
)

func WaitSshReady(id string, port int) error {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer watcher.Close()

	err = watcher.Add(filepath.Join(config.CacheDir, id))
	if err != nil {
		return fmt.Errorf("add watcher: %w", err)
	}

	vmPidFile := filepath.Join(config.CacheDir, id, config.RunPidFile)
	for {
		exists, err := utils.FileExists(vmPidFile)
		if err != nil {
			return fmt.Errorf("check for file: %w", err)
		}

		if exists {
			break
		}

		select {
		case <-watcher.Events:
		case err, ok := <-watcher.Errors:
			if !ok {
				return errors.New("unknown error")
			}
			return fmt.Errorf("watcher event: %w", err)
		}
	}

	for {
		if utils.IsPortOpen(port) {
			return nil
		}
	}
}

func Kill(id string) error {
	vmPidFile := filepath.Join(config.CacheDir, id, config.RunPidFile)
	pid, err := utils.ReadPidFile(vmPidFile)
	if err != nil {
		return fmt.Errorf("reading pid file: %w", err)
	}

	process, err := os.FindProcess(pid)
	if err != nil {
		return fmt.Errorf("finding process: %w", err)
	}

	return process.Signal(os.Interrupt)
}

func Run(id string, sshPort int, user, sshIdentity string, injectKey, ciData bool, ciPort int) error {
	vmDir := filepath.Join(config.CacheDir, id)

	var args []string
	args = append(args, "-accel", "kvm", "-cpu", "host")
	args = append(args, "-m", "2G")
	args = append(args, "-smp", "2")
	nicCmd := fmt.Sprintf("user,model=virtio-net-pci,hostfwd=tcp::%d-:22", sshPort)
	args = append(args, "-nic", nicCmd)
	//args = append(args, "-nographic")

	vmPidFile := filepath.Join(vmDir, config.RunPidFile)
	args = append(args, "-pidfile", vmPidFile)

	vmDiskImage := filepath.Join(vmDir, config.BootcDiskImage)
	driveCmd := fmt.Sprintf("if=virtio,format=raw,file=%s", vmDiskImage)
	args = append(args, "-drive", driveCmd)
	if ciData {
		if ciPort != -1 {
			// http cloud init data transport
			// FIXME: this IP address is qemu specific, it should be configurable.
			smbiosCmd := fmt.Sprintf("type=1,serial=ds=nocloud;s=http://10.0.2.2:%d/", ciPort)
			args = append(args, "-smbios", smbiosCmd)
		} else {
			// cdrom cloud init data transport
			ciDataIso := filepath.Join(vmDir, config.BootcCiDataIso)
			args = append(args, "-cdrom", ciDataIso)
		}
	}

	if injectKey {
		smbiosCmd, err := OemString(user, sshIdentity)
		if err != nil {
			return err
		}

		args = append(args, "-smbios", smbiosCmd)
	}

	cmd := exec.Command("qemu-system-x86_64", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Start()
}
