project = podman-bootc
output_dir = bin

build: build_linux build_darwin

build_linux: bin_dir
	go build -o $(output_dir)/$(project)

build_darwin: bin_dir
	env GOOS=darwin GOARCH=arm64 go build -o $(output_dir)/$(project)-macos

bin_dir:
	mkdir -p bin

clean:
	rm -f $(output_dir)/$(project) $(output_dir)/$(project)-macos

# my local testing setup
-include ../testing.mk