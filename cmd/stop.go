package cmd

import (
	"bootc/pkg/config"
	"bootc/pkg/ssh"

	"github.com/spf13/cobra"
)

var stopCmd = &cobra.Command{
	Use:          "stop ID",
	Short:        "Stop an existing OS Container machine",
	Long:         "Stop an existing OS Container machine",
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
	RunE:         doStop,
}

func init() {
	RootCmd.AddCommand(stopCmd)
}

func doStop(_ *cobra.Command, args []string) error {
	return doStopVm(args[0])
}

func doStopVm(id string) error {

	cfg, err := config.LoadConfig(id)
	if err != nil {
		return err
	}

	poweroff := []string{"poweroff"}
	return ssh.CommonSSH("root", cfg.SshIdentity, id, cfg.SshPort, poweroff)
}
