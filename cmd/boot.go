package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"bootc/pkg/bootc"
	"bootc/pkg/config"
	"bootc/pkg/credentials"
	"bootc/pkg/podman"
	"bootc/pkg/ssh"
	"bootc/pkg/utils"
	"bootc/pkg/vm"

	"github.com/spf13/cobra"
)

type osVmConfig struct {
	Local           bool
	User            string
	SshIdentity     string
	InjSshIdentity  bool
	GenSshIdentity  bool
	CloudInitDir    string
	KsFile          string
	Interactive     bool
	RemoveVm        bool // Kill the running VM when it exits
	RemoveDiskImage bool // After exit of the VM, remove the disk image
}

var (
	// listCmd represents the hello command
	bootCmd = &cobra.Command{
		Use:          "boot",
		Short:        "Boot OS Containers",
		Long:         "Boot OS Containers",
		Args:         cobra.ExactArgs(1),
		RunE:         doBoot,
		SilenceUsage: true,
	}

	vmConfig = osVmConfig{}
)

func init() {
	RootCmd.AddCommand(bootCmd)
	bootCmd.Flags().BoolVarP(&vmConfig.Local, "local", "L", false, "Copy image from local storage")
	bootCmd.Flags().StringVarP(&vmConfig.User, "user", "u", "root", "--user <user name> (default: root)")

	// I don't want to deal with cobra quirks right now, let's use multiple options
	bootCmd.Flags().StringVar(&vmConfig.SshIdentity, "ssh-identity", config.DefaultIdentity, "--ssh-identity <identity file>")
	bootCmd.Flags().BoolVar(&vmConfig.InjSshIdentity, "inj-ssh-identity", false, "--inj-ssh-identity")
	bootCmd.Flags().BoolVar(&vmConfig.GenSshIdentity, "gen-ssh-identity", false, "--gen-ssh-identity (implies --inj-ssh-identity)")

	bootCmd.Flags().StringVar(&vmConfig.CloudInitDir, "cloudinit", "", "--cloudinit [[transport:]cloud-init data directory] (transport: cdrom | imds)")

	bootCmd.Flags().BoolVarP(&vmConfig.Interactive, "interactive", "i", false, "-i")
	bootCmd.Flags().BoolVar(&vmConfig.RemoveVm, "rm", false, "Kill the running VM when it exits, requires --interactive")

	// Unsupported yet
	//bootCmd.Flags().BoolVar(&vmConfig.RemoveDiskImage, "rmi", false, "After exit of the VM, remove the disk image") // TODO: it requires a monitor process
	//bootCmd.Flags().StringVar(&vmConfig.KsFile, "ks", "", "--ks [kickstart file]") // TODO

}

func doBoot(flags *cobra.Command, args []string) error {
	if vmConfig.GenSshIdentity && flags.Flags().Changed("ssh-identity") {
		return fmt.Errorf("incompatible options: --ssh-identity and --gen-ssh-identity")
	}
	idOrName := args[0]

	if !podman.IsDefaultMachineRunning() {
		return errors.New("default machine not running: please execute 'podman machine init && podman machine start'")
	}

	// Pull the image if not present
	// TODO: add --pull policy
	imageId, err := podman.GetOciImage(idOrName, vmConfig.Local)
	if err != nil {
		return err
	}

	// Load image to the podman machine root
	// Sadly we can't share the image storage between the 'root' and the 'core' user
	if err := podman.LoadImageToMachineRoot(imageId, vmConfig.Local); err != nil {
		return err
	}

	// Create VM cache dir
	vmDir := filepath.Join(config.CacheDir, imageId)
	if err := os.MkdirAll(vmDir, os.ModePerm); err != nil {
		return fmt.Errorf("MkdirAll: %w", err)
	}

	// install
	err = bootc.Install(imageId, vmDir)
	if err != nil {
		return fmt.Errorf("installing: %w", err)
	}

	// cloud-init required?
	ciPort := -1 // for http transport
	ciData := flags.Flags().Changed("cloudinit")
	if ciData {
		ciPort, err = vm.SetCloudInit(imageId, vmConfig.CloudInitDir)
		if err != nil {
			return err
		}
	}

	// Generate ssh ssh
	injectSshKey := vmConfig.InjSshIdentity
	if vmConfig.GenSshIdentity {
		injectSshKey = true
		vmConfig.SshIdentity, err = credentials.Generatekeys(vmDir)
		if err != nil {
			return err
		}
	}

	sshPort, err := utils.GetFreeLocalTcpPort()
	if err != nil {
		return fmt.Errorf("ssh getFreeTcpPort: %w", err)
	}

	err = vm.Run(imageId, sshPort, vmConfig.User, vmConfig.SshIdentity, injectSshKey, ciData, ciPort)
	if err != nil {
		return fmt.Errorf("runBootcVM: %w", err)
	}

	// write down the config file
	if err := config.WriteConfig(vmDir, sshPort, vmConfig.SshIdentity); err != nil {
		return err
	}

	// Only for interactive
	if vmConfig.Interactive {
		// wait for VM
		if err := vm.WaitSshReady(imageId, sshPort); err != nil {
			return err
		}

		// ssh into it
		cmd := make([]string, 0)
		if err := ssh.CommonSSH(vmConfig.User, vmConfig.SshIdentity, imageId, sshPort, cmd); err != nil {
			return err
		}

		if vmConfig.RemoveVm {
			if err := vm.Kill(imageId); err != nil {
				return err
			}
		}
	}

	return nil
}
